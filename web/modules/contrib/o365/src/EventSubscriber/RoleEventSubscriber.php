<?php

namespace Drupal\o365\EventSubscriber;

use Drupal\externalauth\Event\ExternalAuthEvents;
use Drupal\externalauth\Event\ExternalAuthLoginEvent;
use Drupal\o365\RolesService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Custom event subscriber to handle roles on login.
 */
class RoleEventSubscriber implements EventSubscriberInterface {

  /**
   * RoleEventSubscriber constructor.
   *
   * @param \Drupal\o365\RolesService $rolesService
   *   The roles service that handles the roles.
   */
  public function __construct(
    protected readonly RolesService $rolesService,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[ExternalAuthEvents::LOGIN][] = ['handleRoles'];
    return $events;
  }

  /**
   * Handle the role logins.
   *
   * @param \Drupal\externalauth\Event\ExternalAuthLoginEvent $event
   *   The ExternalAuthLoginEvent event containing the user.
   */
  public function handleRoles(ExternalAuthLoginEvent $event) {
    if ($event->getProvider() === 'o365_sso') {
      $this->rolesService->handleRoles($event->getAccount());
    }
  }

}
