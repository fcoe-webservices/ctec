<?php

namespace Drupal\o365\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\o365\HelperService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Microsoft 365 Connector routes.
 */
final class O365AuthScopesController extends ControllerBase {

  /**
   * The o365.helpers service.
   *
   * @var \Drupal\o365\HelperService
   */
  protected $helperService;

  /**
   * The controller constructor.
   *
   * @param \Drupal\o365\HelperService $o365_helpers
   *   The o365.helpers service.
   */
  public function __construct(HelperService $o365_helpers) {
    $this->helperService = $o365_helpers;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('o365.helpers'));
  }

  /**
   * Builds the response.
   */
  public function build() {
    $scopes = $this->helperService->getAuthScopes(TRUE);
    sort($scopes);

    $build['content'] = [
      '#theme' => 'o365_auth_scopes_table',
      '#scopes' => $scopes,
    ];

    return $build;
  }

}
