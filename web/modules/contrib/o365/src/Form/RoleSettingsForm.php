<?php

declare(strict_types=1);

namespace Drupal\o365\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Map groups in Microsoft 365 to Drupal roles.
 */
final class RoleSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'o365_role_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['o365.role_settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('o365.role_settings');

    $form['roles_map'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Role mapping'),
      '#description' => $this->t('Put a mapping on each line, in format [Group in O365]|[Drupal role name]|[Another Drupal role name].<br/>For example: <i>O365_TEACHER|teacher</i>. For the group name in Microsoft 365 you can use the id, displayName or onPremisesSamAccountName.'),
      '#default_value' => $config->get('roles_map'),
    ];

    $form['safe_roles'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Safe roles'),
      '#description' => $this->t('These are the roles the module won\'t remove from the logged in user. The "anonymous" and "authenticated" roles are already excluded.'),
      '#default_value' => $config->get('safe_roles'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('o365.role_settings')
      ->set('roles_map', trim($form_state->getValue('roles_map')))
      ->set('safe_roles', $form_state->getValue('safe_roles'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
