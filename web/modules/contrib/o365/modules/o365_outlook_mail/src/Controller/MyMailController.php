<?php

namespace Drupal\o365_outlook_mail\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\o365_outlook_mail\GetMailServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller that renders a list of emails belonging to the logged in user.
 */
final class MyMailController extends ControllerBase {

  /**
   * Drupal\o365_outlook_mail\GetMailServiceInterface definition.
   *
   * @var \Drupal\o365_outlook_mail\GetMailServiceInterface
   */
  protected $getMailService;

  /**
   * Constructs a new MyMailController object.
   *
   * @param \Drupal\o365_outlook_mail\GetMailServiceInterface $getMailService
   *   The GetMailServiceInterface definition.
   */
  public function __construct(GetMailServiceInterface $getMailService) {
    $this->getMailService = $getMailService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('o365_outlook_mail.get_mail'));
  }

  /**
   * Get the latest mails of the user.
   *
   * @return array
   *   The render array with the list of mails.
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   * @throws \League\OAuth2\Client\Provider\Exception\IdentityProviderException
   * @throws \Microsoft\Graph\Exception\GraphException
   * @throws \Exception
   */
  public function getMail() {
    $mailData = $this->getMailService->getMail(20, [
      'from',
      'subject',
      'receivedDateTime',
      'bodyPreview',
      'isRead',
      'webLink',
    ]);

    if ($mailData) {
      $build = $this->getMailService->generateMailList($mailData);
    }
    else {
      $build['content'] = [
        '#markup' => t('No emails have been found.'),
      ];
    }

    return $build;
  }

}
