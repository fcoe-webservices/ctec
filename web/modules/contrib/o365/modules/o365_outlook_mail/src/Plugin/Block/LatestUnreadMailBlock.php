<?php

namespace Drupal\o365_outlook_mail\Plugin\Block;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\o365\Block\O365UncachedBlockBase;
use Drupal\o365\GraphService;
use Drupal\o365_outlook_mail\GetMailServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Latest Unread Mail' block.
 *
 * @Block(
 *   id = "o365_latest_unread_mail",
 *   admin_label = @Translation("Latest Unread Mail"),
 *   category = @Translation("Microsoft 365")
 * )
 */
final class LatestUnreadMailBlock extends O365UncachedBlockBase implements ContainerFactoryPluginInterface {

  /**
   * The mail service interface.
   *
   * @var \Drupal\o365_outlook_mail\GetMailServiceInterface
   */
  protected $getMailService;

  /**
   * Constructs a new LatestMailBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\o365\GraphService $graphService
   *   The o365 Graph service.
   * @param \Drupal\o365_outlook_mail\GetMailServiceInterface $getMailService
   *   The get mail service definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, GraphService $graphService, GetMailServiceInterface $getMailService) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $graphService);
    $this->getMailService = $getMailService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('o365.graph'),
      $container->get('o365_outlook_mail.get_mail')
    );
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   * @throws \League\OAuth2\Client\Provider\Exception\IdentityProviderException
   * @throws \Microsoft\Graph\Exception\GraphException
   */
  public function build() {
    $config = $this->getConfiguration();
    $amount = $config['amount'] ?? 5;

    $mails = $this->getMails($amount);
    if (!empty($mails)) {
      return $mails;
    }

    if (isset($config['show_latest_unread_mail_block']) && !empty($config['show_latest_unread_mail_block'])) {
      return [
        '#theme' => 'o365_outlook_mail_list_block',
        '#readmore_class' => 'button--primary button',
        '#list' => $config['latest_unread_mail_block_text'],
      ];
    }
    return [];
  }

  /**
   * Get and normalize a list of mails.
   *
   * @param int $count
   *   The number of mails to show.
   *
   * @return mixed
   *   The item list or FALSE.
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   * @throws \League\OAuth2\Client\Provider\Exception\IdentityProviderException
   * @throws \Microsoft\Graph\Exception\GraphException
   */
  private function getMails(int $count = 5): mixed {
    $mailData = $this->getMailService->getUnreadMail($count, [
      'from',
      'subject',
      'receivedDateTime',
      'bodyPreview',
      'isRead',
      'webLink',
    ]);

    if ($mailData) {
      return $this->getMailService->generateMailList($mailData, 'block');
    }

    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['amount'] = [
      '#type' => 'number',
      '#title' => $this->t('Amount of mails to show.'),
      '#default_value' => $config['amount'] ?? 5,
      '#min' => 1,
      '#max' => 999,
    ];

    $form['show_latest_unread_mail_block'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Show latest unread mail's when there is no data"),
      '#default_value' => ($config['show_latest_unread_mail_block']) ?? FALSE,
    ];

    $form['latest_unread_mail_block_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Mail block text'),
      '#description' => $this->t('Enter the text you would like to show'),
      '#placeholder' => $this->t('Enter the text you would like to show'),
      '#default_value' => $config['latest_unread_mail_block_text'] ?? $this->t('No unread mails found.'),
      '#states' => [
        'visible' => [
          ':input[name="settings[show_latest_unread_mail_block]"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->configuration['amount'] = $values['amount'];
    $this->configuration['show_latest_unread_mail_block'] = $values['show_latest_unread_mail_block'];
    $this->configuration['latest_unread_mail_block_text'] = $values['latest_unread_mail_block_text'];
  }

}
