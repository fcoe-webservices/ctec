<?php

namespace Drupal\o365_sso_user\Form;

use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Settings form for the o365_sso_user module.
 */
final class SettingsForm extends ConfigFormBase {

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfo|\Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The constructor used for dependency injection.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entityTypeBundleInfo
   *   The entity type bundle info service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ModuleHandlerInterface $moduleHandler, EntityTypeBundleInfoInterface $entityTypeBundleInfo) {
    $this->moduleHandler = $moduleHandler;
    $this->entityTypeBundleInfo = $entityTypeBundleInfo;
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('config.factory'), $container->get('module_handler'), $container->get('entity_type.bundle.info'));
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'o365_sso_user.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Config\ImmutableConfig $config */
    $config = $this->config('o365_sso_user.settings');
    $form['use_graph_data'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use user data from Graph API'),
      '#description' => $this->t('When checked you can map field from the Graph API (/me endpoint) to field names in the user. These values get synced every login.'),
      '#default_value' => $config->get('use_graph_data'),
    ];

    $linkText = $this->t('Microsoft documentation');
    $linkUrl = Url::fromUri('https://docs.microsoft.com/en-us/graph/api/user-get?view=graph-rest-1.0&tabs=http#example-3-use-select-to-retrieve-specific-properties-of-a-user', ['absolute' => TRUE]);
    $link = Link::fromTextAndUrl($linkText, $linkUrl);
    $form['graph_data_selected_fields'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Selected fields from the Graph API'),
      '#description' => $this->t('Input a comma seperated list of fields you would like to get from the GraphApi. You can read the @link for more information about which fields you can select. When no fields are entered a basic set of fields is used: <i>businessPhones, displayName, givenName, id, jobTitle, mail, mobilePhone, officeLocation, preferredLanguage, surname, userPrincipalName</i>. Some required fields are automatically added.', ['@link' => $link->toString()]),
      '#default_value' => $config->get('graph_data_selected_fields'),
      '#states' => [
        'visible' => [
          ':input[name="use_graph_data"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['graph_data_mapping'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Field mapping Graph API > Drupal'),
      '#description' => $this->t('Add a field mapping per line. Use a pipe to separate the field machine name in Graph API and in Drupal. For instance "givenName|field_first_name".'),
      '#default_value' => $config->get('graph_data_mapping'),
      '#states' => [
        'visible' => [
          ':input[name="use_graph_data"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $url = Url::fromUri('https://www.drupal.org/project/profile', ['attributes' => ['target' => '_blank']]);
    $link = Link::fromTextAndUrl('Profile', $url);
    $form['use_profile_module'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use profile instead of basic user.'),
      '#description' => $this->t('Check this checkbox if you use the @link module.', ['@link' => $link->toString()]),
      '#default_value' => $config->get('use_profile_module'),
    ];

    $this->createProfileBundleField($form, $config);

    $form['sync_profile_image'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Sync the user profile picture'),
      '#default_value' => $config->get('sync_profile_image'),
      '#states' => [
        'visible' => [
          ':input[name="use_graph_data"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['profile_image_mapping'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Profile picture field name'),
      '#description' => $this->t('Use the fields machine name.'),
      '#default_value' => $config->get('profile_image_mapping'),
      '#states' => [
        'visible' => [
          ':input[name="sync_profile_image"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = $this->config('o365_sso_user.settings');
    $config->set('use_graph_data', $form_state->getValue('use_graph_data'))
      ->set('graph_data_mapping', $form_state->getValue('graph_data_mapping'))
      ->set('graph_data_selected_fields', $form_state->getValue('graph_data_selected_fields'))
      ->set('use_profile_module', $form_state->getValue('use_profile_module'))
      ->set('sync_profile_image', $form_state->getValue('sync_profile_image'))
      ->set('profile_image_mapping', $form_state->getValue('profile_image_mapping'));

    if ($this->moduleHandler->moduleExists('profile')) {
      $config->set('profile_bundle', $form_state->getValue('profile_bundle'));
    }

    $config->save();
  }

  /**
   * Create the profile bundle field.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Config\Config $config
   *   The config object.
   */
  private function createProfileBundleField(array &$form, Config $config) {
    if ($this->moduleHandler->moduleExists('profile')) {
      $profileBundles = $this->entityTypeBundleInfo->getBundleInfo('profile');
      $options = [];
      foreach ($profileBundles as $key => $label) {
        $options[$key] = $label['label'];
      }

      $form['profile_bundle'] = [
        '#type' => 'select',
        '#title' => $this->t('Select the profile type we need to save data to.'),
        '#options' => $options,
        '#default_value' => $config->get('profile_bundle'),
        '#states' => [
          'visible' => [
            ':input[name="use_profile_module"]' => ['checked' => TRUE],
          ],
        ],
      ];
    }
  }

}
