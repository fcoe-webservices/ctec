<?php

namespace Drupal\o365_links\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\UncacheableDependencyTrait;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\o365_links\O365LinksConstants;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Microsoft 365 Application Links' block.
 *
 * @Block(
 *   id = "o365_links",
 *   admin_label = @Translation("Application Links"),
 *   category = @Translation("Microsoft 365")
 * )
 */
final class O365LinksBlock extends BlockBase implements ContainerFactoryPluginInterface {

  // Make this block not cacheable.
  use UncacheableDependencyTrait;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The o365_links.constants service.
   *
   * @var \Drupal\o365_links\O365LinksConstants
   */
  protected O365LinksConstants $o365LinksConstants;

  /**
   * Constructs a new O365LinksBlock instance.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\o365_links\O365LinksConstants $o365_links_constants
   *   The o365_links.constants service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory, O365LinksConstants $o365_links_constants) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
    $this->o365LinksConstants = $o365_links_constants;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition, $container->get('config.factory'), $container->get('o365_links.constants'));
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $config = $this->configFactory->get('o365_links.settings');
    $newWindow = $config->get('new_window');
    $list = [];

    $officeLinks = $this->o365LinksConstants->getApplications();
    foreach ($officeLinks as $name => $value) {
      $configKey = 'o365_links_' . $name;
      $enabled = $config->get($configKey);
      if (!empty($enabled)) {
        $renderArray = [
          '#type' => 'link',
          '#title' => [
            '#theme' => 'o365_links_application',
            '#key' => $name,
            '#label' => $value['label'],
          ],
          '#url' => Url::fromUri($value['url']),
          '#attributes' => [
            'class' => ['o365-links-application-link'],
          ],
        ];

        if (!empty($newWindow)) {
          $renderArray['#attributes']['target'] = '_blank';
          $renderArray['#attributes']['title'] = new TranslatableMarkup('@label - This link opens in a new window', ['@label' => $value['label']]);
        }

        $list[] = $renderArray;
      }
    }

    if (!empty($list)) {
      $build['content'] = [
        '#attached' => [
          'library' => [
            'o365/icons',
            'o365_links/frontend',
          ],
        ],
        '#theme' => 'item_list',
        '#items' => $list,
        '#attributes' => [
          'class' => ['o365-links-applications'],
        ],
      ];
      return $build;
    }

    return [];
  }

}
