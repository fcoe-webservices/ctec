<?php

namespace Drupal\o365_contacts\Plugin\Block;

use Drupal\o365\Block\O365BlockBase;

/**
 * Provides a 'Contact Search' block.
 *
 * @Block(
 *   id = "o365_contacts",
 *   admin_label = @Translation("Contact Search"),
 *   category = @Translation("Microsoft 365")
 * )
 */
class SearchContactBlock extends O365BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    return \Drupal::formBuilder()->getForm('\Drupal\o365_contacts\Form\SearchContactForm');
  }

}
