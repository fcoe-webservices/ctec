<?php

namespace Drupal\o365_profile\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Microsoft 365 - Profiles and personas settings for this site.
 */
class O365ProfileTeamsLinksConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'o365_profile_o365_profile_teams_links_config';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['o365_profile.teams_links_config'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('o365_profile.teams_links_config');
    $form['ms_teams_call'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable call via Teams button'),
      '#default_value' => $config->get('ms_teams_call'),
    ];

    $form['ms_teams_chat'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable chat via Teams button'),
      '#default_value' => $config->get('ms_teams_chat'),
    ];

    $form['ms_teams_video'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable video call via Teams button'),
      '#default_value' => $config->get('ms_teams_video'),
      '#access' => FALSE,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('o365_profile.teams_links_config')
      ->set('ms_teams_call', $form_state->getValue('ms_teams_call'))
      ->set('ms_teams_chat', $form_state->getValue('ms_teams_chat'))
      ->set('ms_teams_video', $form_state->getValue('ms_teams_video'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
