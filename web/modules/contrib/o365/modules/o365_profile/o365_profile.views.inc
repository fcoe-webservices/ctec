<?php

/**
 * @file
 * This file contains the views hooks used by this module.
 */

/**
 * Implements hook_views_data().
 */
function o365_profile_views_data() {
  $data['o365_profile']['table']['group'] = t('Microsoft 365 Connector - Profile');
  $data['o365_profile']['table']['join'] = [
    '#global' => [],
  ];

  $data['o365_profile']['o365_profile_teams_call'] = [
    'title' => t('Call/chat via Teams'),
    'help' => t('Generate a link to call or chat a user via MS Teams'),
    'field' => [
      'id' => 'o365_profile_teams_call',
    ],
  ];

  return $data;
}
