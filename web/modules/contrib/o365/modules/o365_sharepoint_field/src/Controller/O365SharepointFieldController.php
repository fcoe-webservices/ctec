<?php

namespace Drupal\o365_sharepoint_field\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\o365\GraphService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Returns responses for Microsoft 365 - SharePoint search Field routes.
 */
final class O365SharepointFieldController extends ControllerBase {

  /**
   * The o365.graph service.
   *
   * @var \Drupal\o365\GraphService
   */
  protected $o365Graph;

  /**
   * The node storage.
   *
   * @var \Drupal\node\NodeStorage
   */
  protected $nodeStorage;

  /**
   * The controller constructor.
   *
   * @param \Drupal\o365\GraphService $o365_graph
   *   The o365.graph service.
   */
  public function __construct(GraphService $o365_graph) {
    $this->o365Graph = $o365_graph;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('o365.graph'));
  }

  /**
   * Handler for autocomplete request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response object.
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \League\OAuth2\Client\Provider\Exception\IdentityProviderException
   */
  public function handleAutocomplete(Request $request) {
    $sharepointData = $this->getData($request);

    $list = [];
    if ($sharepointData) {
      foreach ($sharepointData['value'] as $value) {
        foreach ($value['hitsContainers'] as $hitsContainers) {
          foreach ($hitsContainers['hits'] as $hits) {
            $hitId = $hits['hitId'];
            $driveId = $hits['resource']["parentReference"]["driveId"];

            $list[] = [
              'value' => $hits['resource']['name'],
              'label' => $hits['resource']['name'],
              'hitId' => $hitId,
              'driveId' => $driveId,
            ];
          }
        }
      }
    }

    return new JsonResponse($list);
  }

  /**
   * Handler for the CKEditor autocomplete.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response object.
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \League\OAuth2\Client\Provider\Exception\IdentityProviderException
   */
  public function handleCkEditorAutocomplete(Request $request) {
    $sharepointData = $this->getData($request, ' AND isDocument=true');

    $list = [];
    if ($sharepointData) {
      foreach ($sharepointData['value'] as $value) {
        foreach ($value['hitsContainers'] as $hitsContainers) {
          foreach ($hitsContainers['hits'] as $hits) {
            $list[] = [
              'value' => $hits['resource']['webUrl'],
              'label' => $hits['resource']['name'],
            ];
          }
        }
      }
    }

    return new JsonResponse($list);
  }

  /**
   * Get the data from sharepoint.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param string $scope
   *   The scope, with this we can filter out folders.
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \League\OAuth2\Client\Provider\Exception\IdentityProviderException
   */
  protected function getData(Request $request, $scope = '') {
    $input = $request->query->get('q');
    $input = Xss::filter($input);

    $jsondata = [
      'requests' => [
        0 => [
          'entityTypes' => [
            0 => 'driveItem',
          ],
          'query' => [
            'queryString' => $input . $scope,
          ],
          'size' => 10,
        ],
      ],
    ];

    return $this->o365Graph->sendGraphData('/search/query/', $jsondata);
  }

}
