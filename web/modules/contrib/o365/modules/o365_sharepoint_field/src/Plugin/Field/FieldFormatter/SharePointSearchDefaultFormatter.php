<?php

namespace Drupal\o365_sharepoint_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Url;

/**
 * Plugin implementation 'sharepoint_search_link' formatter.
 *
 * @FieldFormatter(
 *   id = "sharepoint_search_link",
 *   label = @Translation("Microsoft 365 - SharePoint Search link"),
 *   field_types = {"sharepoint_search_link"}
 * )
 */
class SharePointSearchDefaultFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      $values = $item->getValue();
      if ($item->sharepoint_file_name) {
        $element[$delta]['field_sharepoint_search'] = [
          '#type' => 'link',
          '#title' => $values["sharepoint_file_name"],
          '#url' => Url::fromUri($values['sharepoint_web_url']),
        ];

      }

    }

    return $element;
  }

}
