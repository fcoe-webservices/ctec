<?php

namespace Drupal\o365_sso\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\o365\GraphService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Custom logout controller for o365_sso module.
 */
class LogoutController extends ControllerBase {

  /**
   * The module config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The o365 Graph service.
   *
   * @var \Drupal\o365\GraphService
   */
  protected $graphService;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * The controller constructor.
   */
  public function __construct(ConfigFactoryInterface $configFactory, GraphService $o365Graph, AccountInterface $account) {
    $this->config = $configFactory->get('o365_sso.settings');
    $this->graphService = $o365Graph;
    $this->account = $account;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('config.factory'),
      $container->get('o365.graph'),
      $container->get('current_user')
    );
  }

  /**
   * Logout a user and redirect to office if needed.
   */
  public function logout() {
    $currentUserId = $this->graphService->getCurrentUserId();
    $logoutFromOffice = $this->config->get('logout_office');
    $url = Url::fromRoute('user.logout', [], ['absolute' => TRUE])->toString(TRUE);

    // Logout the user from Drupal.
    user_logout();

    // Logout from Office if we want to.
    if (!empty($logoutFromOffice) && !empty($currentUserId)) {
      // Redirect the user to office to logout from office.
      $response = new TrustedRedirectResponse('https://login.microsoftonline.com/common/oauth2/logout?post_logout_redirect_uri=' . $url->getGeneratedUrl());
    }
    else {
      // Redirect to homepage.
      $response = new TrustedRedirectResponse($url->getGeneratedUrl());
    }

    // Return the cacheable redirect.
    $response->addCacheableDependency($url);
    $response->addCacheableDependency($this->account);
    return $response;
  }

}
