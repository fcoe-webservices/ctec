<?php

namespace Drupal\o365_sso\Routing;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Auto redirect users when the auto redirect option is enabled.
 */
class UserLoginFormRouteSubscriber extends RouteSubscriberBase {

  /**
   * The module config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  private $config;

  /**
   * The class constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   */
  public function __construct(
    private readonly ConfigFactoryInterface $configFactory,
  ) {
    $this->config = $this->configFactory->get('o365_sso.settings');
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // Disable login form page cache when the auto redirect option is enabled.
    if (!empty($this->config->get('auto_redirect')) && $route = $collection->get('user.login')) {
      $options = $route->getOptions();
      $options['no_cache'] = TRUE;
      $route->setOptions($options);
    }
  }

}
