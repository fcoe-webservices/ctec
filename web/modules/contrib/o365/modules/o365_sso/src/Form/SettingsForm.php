<?php

namespace Drupal\o365_sso\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Settings form for the SSO module.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'o365_sso.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('o365_sso.settings');

    $form['fs_login'] = [
      '#type' => 'details',
      '#title' => $this->t('Login / logout settings'),
      '#open' => TRUE,
    ];

    $form['fs_login']['link_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Link text'),
      '#default_value' => $config->get('link_text') ?? 'Login with Microsoft 365',
      '#description' => $this->t('This text will be used as the text in the link.'),
      '#required' => TRUE,
    ];

    $form['fs_login']['link_position'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show the Office login link on top of the login form'),
      '#default_value' => $config->get('link_position'),
      '#description' => $this->t('When disabled the link to the Office login will be shown on the bottom of the login form.'),
    ];

    $form['fs_login']['hide_login_form'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide the login form'),
      '#default_value' => $config->get('hide_login_form'),
      '#description' => $this->t('When enabled nog login form will be shown. This can be overridden with ?show_login=true query parameter.'),
    ];

    $form['fs_login']['only_existing_users'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable creating new users'),
      '#default_value' => $config->get('only_existing_users'),
      '#description' => $this->t('When checked only existing users are able to log in (matched on e-mail address). A error will be shown to users without a Drupal user.'),
    ];

    $form['fs_login']['logout_office'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log out from Microsoft 365'),
      '#default_value' => $config->get('logout_office'),
      '#description' => $this->t('When checked a user will also automatically be logged out of office when logging out of Drupal.'),
    ];

    $form['fs_redirects'] = [
      '#type' => 'details',
      '#title' => $this->t('Redirect settings'),
      '#open' => TRUE,
    ];

    $form['fs_redirects']['auto_redirect'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Automatically redirect users to the SSO login page'),
      '#default_value' => $config->get('auto_redirect'),
      '#description' => $this->t('When enabled anonymous users that access the /user/login page will automatically be redirected to the SSO login page. Users that use "local" users (like admins) can use the /user/drupal_login path to use the normal Drupal login.'),
    ];

    $form['fs_redirects']['redirect_login_destination'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Redirect after login to destination URL'),
      '#description' => $this->t('After login users will be redirected to the destination page.'),
      '#default_value' => $config->get('redirect_login_destination'),
    ];

    $form['fs_user'] = [
      '#type' => 'details',
      '#title' => $this->t('User settings'),
      '#open' => TRUE,
    ];

    $form['fs_user']['hide_user_fields'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide the username, password and e-mail fields from the user edit form'),
      '#default_value' => $config->get('hide_user_fields'),
      '#description' => $this->t('This is only applicable to users that have been added with the Microsoft 365 connector module.'),
    ];

    $form['fs_user']['update_username'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Update username on every login'),
      '#default_value' => $config->get('update_username'),
      '#description' => $this->t('When checked the username will be updated on every login based on the username attribute.'),
    ];

    $form['fs_user']['username_attribute'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username property'),
      '#default_value' => $config->get('username_attribute') ?? 'displayName',
      '#description' => $this->t('The property of the Microsoft 365 user to use as the username.'),
      '#required' => TRUE,
    ];

    $form['fs_advanced'] = [
      '#type' => 'details',
      '#title' => $this->t('Advanced settings'),
    ];

    $options = [
      'businessPhones' => 'businessPhones',
      'displayName' => 'displayName',
      'givenName' => 'givenName',
      'jobTitle' => 'jobTitle',
      'mail' => 'mail',
      'mobilePhone' => 'mobilePhone',
      'officeLocation' => 'officeLocation',
      'preferredLanguage' => 'preferredLanguage',
      'surname' => 'surname',
      'userPrincipalName' => 'userPrincipalName',
      'id' => 'id',
      'other' => 'other',
    ];

    $form['fs_advanced']['email_property'] = [
      '#type' => 'select',
      '#title' => $this->t('E-mail property'),
      '#default_value' => $config->get('email_property') ?? 'userPrincipalName',
      '#description' => $this->t('When creating new users we need a value to use as the e-mail address. In most cases "userPrincipalName" will be sufficient, but in some cases we need a different attribute like "mail"'),
      '#options' => $options,
      '#required' => TRUE,
    ];

    $url = Url::fromUri('https://learn.microsoft.com/nl-nl/graph/api/resources/user?view=graph-rest-1.0#properties', ['attributes' => ['target' => '_blank']]);
    $link = Link::fromTextAndUrl('https://learn.microsoft.com/nl-nl/graph/api/resources/user?view=graph-rest-1.0#properties', $url);
    $form['fs_advanced']['email_property_other'] = [
      '#type' => 'textfield',
      '#title' => $this->t('E-mail property (other)'),
      '#default_value' => $config->get('email_property_other') ?? '',
      '#description' => $this->t('If the property is not available in the list above, you can add your own specific property. Take a look at @link for the available properties of a user.', ['@link' => $link->toString()]),
      '#states' => [
        'visible' => [
          ':input[name="email_property"]' => ['value' => 'other'],
        ],
        'required' => [
          ':input[name="email_property"]' => ['value' => 'other'],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('o365_sso.settings')
      ->set('auto_redirect', $form_state->getValue('auto_redirect'))
      ->set('link_text', $form_state->getValue('link_text'))
      ->set('link_position', $form_state->getValue('link_position'))
      ->set('redirect_login_destination', $form_state->getValue('redirect_login_destination'))
      ->set('hide_user_fields', $form_state->getValue('hide_user_fields'))
      ->set('hide_login_form', $form_state->getValue('hide_login_form'))
      ->set('only_existing_users', $form_state->getValue('only_existing_users'))
      ->set('logout_office', $form_state->getValue('logout_office'))
      ->set('update_username', $form_state->getValue('update_username'))
      ->set('username_attribute', $form_state->getValue('username_attribute'))
      ->set('email_property', $form_state->getValue('email_property'))
      ->set('email_property_other', $form_state->getValue('email_property_other'))
      ->save();
  }

}
