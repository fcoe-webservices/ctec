<?php

namespace Drupal\o365_teams\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\o365_teams\O365TeamsSendMessageService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides an Microsoft 365 - Teams integration form.
 */
final class O365TeamsSendMessageForm extends FormBase {

  /**
   * Our custom message sending service.
   *
   * @var \Drupal\o365_teams\O365TeamsSendMessageService
   */
  protected $sendMessageService;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  protected $request;

  /**
   * The form class constructor.
   *
   * @param \Drupal\o365_teams\O365TeamsSendMessageService $sendMessageService
   *   Our custom message sending service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   *   The current request stack.
   */
  public function __construct(O365TeamsSendMessageService $sendMessageService, RequestStack $request) {
    $this->sendMessageService = $sendMessageService;
    $this->request = $request->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('o365_teams.send_message_service'), $container->get('request_stack'));
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'o365_teams_o365_teams_send_message';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['message_container'] = [
      '#type' => 'container',
    ];

    // When modal  === 'true' we will use some different form of form.
    $modal = $this->request->get('modal');
    $form_state->set('modal', $modal);

    // When redirect is filled, we will redirect to the specified route.
    $redirect = $this->request->get('redirect');
    $form_state->set('redirect', $redirect);

    $defaultRecipient = '';
    if (!empty($this->request->get('to'))) {
      $user = $this->sendMessageService->getUserDataFromRecipient($this->request->get('to'));
      $defaultRecipient = $user->getDisplayName() . ' (' . $user->getUserPrincipalName() . ')';
    }

    $form['message_container']['recipient'] = [
      '#type' => ($modal === 'true' && !empty($defaultRecipient)) ? 'hidden' : 'textfield',
      '#title' => $this->t('Recipient'),
      '#autocomplete_route_name' => 'o365_teams.autocomplete.teams_recipients',
      '#required' => TRUE,
      '#default_value' => $defaultRecipient,
    ];

    $form['message_container']['message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message'),
      '#required' => TRUE,
    ];

    if (!empty($this->request->get('to'))) {
      $linkTitle = new TranslatableMarkup('This link opens in a new window');
      $url = Url::fromUri('https://teams.microsoft.com/l/chat/0/0?users=' . $this->request->get('to'), [
        'attributes' => [
          'target' => '_blank',
          'title' => $linkTitle,
        ],
      ]);
      $text = new TranslatableMarkup('Open a chat with @name in Microsoft Teams', ['@name' => $user->getDisplayName()]);
      $link = Link::fromTextAndUrl($text, $url);
      $form['message_container']['teams_link'] = $link->toRenderable();
    }

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $recipient = $this->sendMessageService->sendMessageToUser($form_state->getValue('recipient'), $form_state->getValue('message'));
    $route = 'o365_teams.send_message';
    $params = ['to' => $recipient];

    if ($form_state->get('modal') === 'true') {
      $route = $form_state->get('redirect');
      $params = [];
    }

    $form_state->setRedirect($route, $params);
  }

}
