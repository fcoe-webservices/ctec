<?php

namespace Drupal\o365_sharepoint_file\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\o365\GraphService;
use Mimey\MimeTypes;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Returns responses for o365_sharepoint_file routes.
 */
final class O365SharepointController extends ControllerBase {

  /**
   * Drupal\o365\GraphService definition.
   *
   * @var \Drupal\o365\GraphService
   */
  protected $o365Graph;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  protected $request;

  /**
   * The mimey object.
   *
   * @var \Mimey\MimeTypes
   */
  protected $mimey;

  /**
   * Constructs a new TeamsGroup object.
   *
   * @param \Drupal\o365\GraphService $o365_graph
   *   The GraphService definition.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The RequestStack definition.
   */
  public function __construct(GraphService $o365_graph, RequestStack $requestStack) {
    $this->o365Graph = $o365_graph;
    $this->request = $requestStack->getCurrentRequest();
    $this->mimey = new MimeTypes();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('o365.graph'), $container->get('request_stack'));
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = $this->formBuilder()
      ->getForm('\Drupal\o365_sharepoint_file\Form\SearchSharepointForm');
    $input = $this->request->get('keyword');
    $items = [];
    $build = [];

    if ($input == NULL) {
      $further = NULL;
    }
    else {
      $further = 'https://www.office.com/search/files?auth=2&q=' . $input;
    }

    if (!empty($input)) {
      $jsondata = [
        'requests' => [
          0 => [
            'entityTypes' => [
              0 => 'driveItem',
            ],
            'query' => [
              'queryString' => $input,
            ],
            'size' => 20,
          ],
        ],
      ];

      $sharepointData = $this->o365Graph->sendGraphData('/search/query/', $jsondata);

      foreach ($sharepointData['value'] as $value) {
        foreach ($value['hitsContainers'] as $hitsContainers) {
          foreach ($hitsContainers['hits'] as $hits) {

            $urlAndExtension = $this->getFileUrlAndExtension($hits);

            $items[] = [
              '#theme' => 'o365_sharepoint_files',
              '#fileName' => pathinfo($hits['resource']['name'], PATHINFO_FILENAME),
              '#fileUrl' => $urlAndExtension['webUrl'] ?? '',
              '#fileExtension' => $urlAndExtension['extension'] ?? '',
              '#lastModifiedBy' => $hits['resource']['lastModifiedBy']['user']['displayName'] ?? '',
              '#lastModifiedDateTime' => date('F j, Y', strtotime($hits['resource']['fileSystemInfo']['lastModifiedDateTime'])),
            ];
          }
        }
      }
    }

    $build['content'] = [
      '#attached' => [
        'library' => [
          'o365/icons',
          'o365_sharepoint_file/sharepoint_file',
        ],
      ],
      '#theme' => 'o365_sharepoint_file_search',
      '#form' => $form,
      '#results' => [
        '#theme' => 'item_list',
        '#items' => $items,
      ],
      '#searchFurther' => $further,
    ];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  private function getFiles($driveId, $fileId) {

    $fileData = $this->o365Graph->getGraphData('/drives/' . $driveId . '/items/' . $fileId);

    return $fileData;
  }

  /**
   * {@inheritdoc}
   */
  private function getFileUrlAndExtension($hit) {
    $data = [];

    $fileId = $hit['resource']['id'] ?? '';
    $driveId = $hit['resource']['parentReference']['driveId'] ?? '';
    $fileData = $this->getFiles($driveId, $fileId);

    $data['webUrl'] = $fileData['webUrl'];

    if (isset($fileData['file'])) {
      $mimeType['file']['mimeType'] = $fileData['file']['mimeType'];
      $data['extension'] = $this->mimey->getExtension($mimeType['file']['mimeType']);
    }
    else {
      $data['extension'] = 'folder';
    }

    return $data;

  }

}
